import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.firefox.FirefoxDriver;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import static org.testng.Assert.fail;

public class PopularNews 
{
	private static WebDriver driver = null;
	
	//Init lists for elements
	private static java.util.List<WebElement> newsList = null;
	private static java.util.List<WebElement> pList = null;
	private static java.util.List<WebElement> aList = null;
	private static java.util.List<WebElement> linkList = null;
	
	//Micromanagement for extra reporter messages
	Boolean defectFound = false;
	
	//Execute before tests
	@BeforeSuite
	public void beforeSuit() 
	{
		//Reference the path of geckodriver for Firefox
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");

		//Take instance of FirefoxDriver
		driver = new FirefoxDriver();

		//Maximize windows after browser opened
		driver.manage().window().maximize();

		//Navigate to the web site
		driver.get("https://techcrunch.com/");		

		//Switch to "Popular" tab
		driver.findElement(By.className("popular")).click();		

		//Get every news block as a web element and add them to the list
		newsList = driver.findElements(By.className("river-block"));

		//Print out list size, just a debug
		System.out.println("News Count: " + newsList.size());
	}
	
	//Check each news summary for names/presence of authors
	@Test(priority = 0)
	public void newsAuthor()
	{			
		WebElement authorLine = null;
		String authorName = null;

		//Check every news block for an author name
		for (int i = 0; i < newsList.size(); i++)
		{			
			try
			{
				authorLine = newsList.get(i).findElement(By.className("byline"));
				authorName = authorLine.findElement(By.tagName("a")).getText();
			}
			catch(Exception e) //Throw exception if there is structural change/error
			{
				Reporter.log("Verification for Authors has failed!");
				fail("Verification for Authors has failed! Page element couldn't be found!");				
			}
			
			//Author name may be null (system) and not entered by poster (client/enduser) so check it
			if (authorName == "" || authorName == null)
			{
				//Show ID of related news that doesn't have author for better bug reporting
				Reporter.log("News with ID: " + newsList.get(i).getAttribute("id") + " doesn't have an author!<br>");
				defectFound = true;
			}
			else
			{
				//Show author order and name for reporting purposes
				Reporter.log(i+1 + " - " + authorName + "<br>");
			}	
		}

		if (defectFound)
		{
			Reporter.log("Verification for Authors has failed!");
		}
		else
		{
			Reporter.log("Verification for Authors has passed!");
		}	
		
		//Fail test if any defect found
		Assert.assertFalse(defectFound, "Verification for Authors has failed! There are news without authors!");			
	}
	
	//Check summaries of news that if they have an image on it's content area
	@Test(priority = 1)
	public void newsImage()
	{		
		defectFound = false;

		WebElement img = null;

		//Check every news block for image
		for (int i = 0; i < newsList.size(); i++)
		{
			try
			{	//If there is an image, show it's source and order number
				img = newsList.get(i).findElement(By.tagName("img"));
				Reporter.log(i+1 + " - " + img.getAttribute("src").toString() + "<br>");
			}
			catch(Exception e)
			{	//If an element with tagName "img" can't be found, throw a defect warning. Give post's ID for devs.
				Reporter.log("News with ID: " + newsList.get(i).getAttribute("id") + " doesn't have an image!<br>");
				defectFound = true;
			}			
		}

		if (defectFound)
		{
			Reporter.log("Verification for Images has failed!");
		}
		else
		{
			Reporter.log("Verification for Images has passed!");
		}
		
		//Fail test if any defect found
		Assert.assertFalse(defectFound, "Verification for Authors has failed! There are news without images!");		
	}
	
	//Check the title of news' content and page title
	@Test(priority = 2)
	public void newsTitle()
	{	
		//Navigate to latest post's content page
		WebElement newsTitle = newsList.get(0).findElement(By.className("post-title")).findElement(By.tagName("a"));
		newsTitle.click();
		
		//Get page title
		String pageTitle = driver.getTitle();
		
		//Some pages has "| TechCrunch" suffix, remove them for failsafe
		if (pageTitle.contains("| TechCrunch"))
		{
			pageTitle = pageTitle.substring(0, pageTitle.length() - 13);
		}

		String contentTitle = driver.findElement(By.tagName("h1")).getText();
		
		//Show both title for a manual comparison
		Reporter.log("PT: " + pageTitle + "<br>" + "CT: " + contentTitle + "<br>");
		//Reporter.log("PTL: " + pageTitle.length() + "<br>" + "CTL: " + contentTitle.length() + "<br>");
		
		//Pass or fail test with comparing to values. They should be exactly same
		if (pageTitle.equals(contentTitle))
		{			
			Reporter.log("Verification for Page & Content Title has passed!");
		}
		else
		{
			Reporter.log("Verification for Page & Content Title has failed!");
			fail("Verification for Page & Content Title has failed! They are not identical!");
		}
	}

	//Check post's content for broken links.
	@Test(priority = 3)
	public void newsLinks() throws IOException
	{
		defectFound = false;
		
		//Get each paragraph of content and list them
		pList = driver.findElement(By.className("l-main")).findElements(By.tagName("p"));
		
		aList = new ArrayList<WebElement>();
		
		//Get each anchor on a specific paragraph and gather them in a list
		for (int i = 0; i < pList.size(); i++)	 
		{		
			aList.addAll(pList.get(i).findElements(By.tagName("a")));
		}		
		
		linkList = new ArrayList<WebElement>();

		for (int i = 0; i < aList.size(); i++)	 
		{	
			//Check each anchor for a proper link and fill them in list
			if(aList.get(i).getAttribute("href") != null)
			{
				linkList.add(aList.get(i));
				//Reporter.log(aList.get(i).getAttribute("href") + "<br>");
			}
		}
		
		//Show the count of total links on content area
		Reporter.log("Total Links on Page: " + linkList.size() + "<br>");

		for (int i = 0; i < linkList.size(); i++)	 
		{			 
			String url = aList.get(i).getAttribute("href");
			
			//Init a http client and send a request to the URL for each item in the link list
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(url);

			try 
			{
				HttpResponse response = client.execute(request);
				
				//If respons code is not 200, throw defect warning
				if (response.getStatusLine().getStatusCode() != 200)
				{
					defectFound = true;
					Reporter.log("FAILED | " + aList.get(i).getAttribute("href") + "<br>");
				}
				else
				{
					Reporter.log("OK | " + aList.get(i).getAttribute("href") + "<br>");
				}
					
			} 
			catch (Exception e) //Catch connection based problems
			{
				e.printStackTrace();
			}			
		}

		//Pass or fail test if there is any broken link
		if (defectFound)
		{
			Reporter.log("Verification for Content Links has failed!");
			fail("Verification for Content Links has failed! There are broken links on the page!");
		}
		else
		{
			Reporter.log("Verification for Page & Content Title has passed!");
		}
	}
	
	//Execute after running all tests
	@AfterSuite
	public void afterSuit()
	{
		//Namesake
		driver.quit();
	}
}